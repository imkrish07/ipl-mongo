const mongoose = require('mongoose');
const ipl = require('./models/match.js');
const csv = require('csvtojson')
const delivery = require('./models/dele.js')
mongoose.Promise = global.Promise;


mongoose.connect('mongodb://127.0.0.1:27017/iplStats');
mongoose.connection.once('open', function() {
    console.log("connection is done");
}).on('error', function(error) {
    console.log('connection error', error);
});
csv().fromFile('ipl/matches.csv')
    .on('json', (jsonObj) => {
        ipl.create(jsonObj).then(function(data) {})
    })
    .on('done', (error) => {
        console.log(error)
    })
csv().fromFile('ipl/deliveries.csv')
    .on('json', (jsonObj) => {
        delivery.create(jsonObj).then(function(data) {})
    })
    .on('done', (error) => {
        console.log(error)
    });