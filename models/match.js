const mongoose = require('mongoose')

const schema = mongoose.Schema;

const matchSchema = new schema(

    {

        id: Number,

        season: Number,

    });

const match = mongoose.model('matches', matchSchema);

module.exports = match;

// module.exports.getMatch = function(callback, limit) {
//     match.find(callback).limit(limit);
// }