const mongoose = require('mongoose')

const schema = mongoose.Schema;

const deleSchema = new schema(

    {

        match_id: Number,

        batting_team: String,

        bowling_team: String,

        batsman: String,

        bowler: String,

        bye_runs: Number,

        legbye_runs: Number,

        total_runs: Number,

        player_dismissed: String,

        dismissal_kind: String,
        batsman_runs: Number

    });

const match = mongoose.model('deliveries', deleSchema);



module.exports = match;