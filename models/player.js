const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var playerSchema = new Schema({
    id: Number,
    match_id: Number,
    season: Number,
    batsman: String,
    batting_team: String,
    bowling_team: String,
    batsman_run: Number,
    bowler: String,
    extra_runs: Number,
    player_dismissed: String,
})

var Player = mongoose.model("player", playerSchema);
module.exports = Player;