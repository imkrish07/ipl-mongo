const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');
const router = require('./app');
var app = express();
mongoose.connect('mongodb://localhost/iplStats');
mongoose.Promise = global.Promise;


var publicPath = path.join(__dirname, './public');
app.use(express.static(publicPath));

app.use('/api', router);

app.listen(4000, function() {
    console.log("listening.......");

})