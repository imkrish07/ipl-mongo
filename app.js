const express = require('express');
const mongoose = require('mongoose');
const match = require('./models/match');
const deli = require('./models/dele');
const player = require('./models/player');

var app = express();
const router = express.Router();
var conn = mongoose.connection;

router.get('/year', function(req, res) {
    match.distinct('season').then((data) => {
        res.send(data.sort())
    });
});
router.get('/team', function(req, res) {
    player.distinct('batting_team', { season: parseInt(req.query.year) }).then((data) => {
        res.send(data)
    });
});
router.get('/teamplayer', function(req, res) {
    var year = req.query.year;
    player.aggregate([{ $match: { season: parseInt(year), batting_team: req.query.team } }, { $group: { "_id": "$batsman" } }]).then((data) => {
        res.send(data)
    });
});
router.get('/playerStats', function(req, res) {
    var year = req.query.year;
    var playerName = req.query.name;

    player.aggregate([{ $match: { season: parseInt(year), batsman: playerName } }, { $group: { "_id": "$batsman", total: { $sum: "$batsman_runs" } } }])
        .then((data) => {
            res.send(data)
        });
});
router.get('/merge', function(req, res) {
    deli.aggregate([{
            $lookup: {
                from: "matches",
                localField: "match_id",
                foreignField: "id",
                as: "player"
            }

        }, {
            $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ["$player", 0] }, "$$ROOT"] } }
        }, { $project: { player: 0 } },
        { $out: "players" }
    ]).then((data) => {
        res.send(data);
    });
});

module.exports = router;