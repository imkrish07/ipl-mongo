fetch('./api/year').then(function(res) {
    return res.json();
}).then(function(data) {
    season(data);
})

function season(data) {
    data.map(function(data1) {
        $('#navtree').append('<ul class=' + 'season' + '>' + data1 + ' </ul>');
    });

    $(document.body).on('click', '.season', function(event) {
        event.stopPropagation();
        var current = $(this);
        var year = $(this).text();
        if ($('.team').length === 0) {
            fetch('./api/team?year=' + year).then(function(resp) {
                return resp.json();
            }).then(function(teams) {
                var html = "";
                teams.map(function(team) {
                    html += ('<ul class="team">' + team + '</ul>');
                });
                current.append(html);
            });
        }

    });

    $(document.body).on('click', '.team', function(event) {
        event.stopPropagation();
        var current = $(this);
        var year = $(this).parent().text().split(' ');
        var teamName = $(this).text();
        fetch('./api/teamplayer?year=' + year[0] + '&team=' + teamName).then(function(resp) {
            return resp.json();
        }).then(function(teams) {
            var html = "";
            teams.map(function(team) {
                html += ('<li class="player">' + team["_id"] + '</li>');
            });
            current.append(html);
        });
    });
    $(document.body).on('click', '.player', function(event) {
        event.stopPropagation();
        var current = $(this);
        var year = $(this).parent().parent().text().split(" ");
        var teamName = $(this).text();;
        fetch('./api/playerStats?year=' + year[0] + '&name=' + teamName).then(function(resp) {
            return resp.json();
        }).then(function(teams) {
            var html = "";
            teams.map(function(team) {
                html += ('<p>' + "Batting Run   " + team["total"] + '</p>');
            });
            current.append(html);
        });
    });
    $('#navtree ul ul li ').children().hide();
    $(document.body).on('click', '.player,.team,.season', function(e) {
        e.stopPropagation();
        $(this).children().slideToggle();

    })
}